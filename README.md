# Introdução ao Projeto

Nosso cliente está muito feliz com o Sistema de Galpões e nos procurou para tratar de um novo problema: em todos os galpões existem itens que já saíram de linha ou possuem pequenos defeitos e que, por isso, não podem mais ser comercializados nas redes de varejo e outros estabelecimentos.

Neste projeto você irá desenvolver uma aplicação web com Ruby on Rails que servirá para conectar o público em geral com o estoque de itens abandonados, permitindo que estes itens sejam comercializados com preços atrativos e que, ao mesmo tempo, os galpões tenham seus espaços melhor aproveitados. O formato escolhido pelo nosso cliente é o de leilão de itens.

O escopo do projeto envolve dois perfis de usuário: administradores responsáveis pelo cadastro de produtos que estão disponíveis para venda, pela gestão do leilão incluindo a configuração de lotes, datas e lances mínimos e, por último, por acompanhar os eventuais pedidos recebidos. Já usuários regulares, que também chamaremos de visitantes, poderão criar uma conta na plataforma, buscar por produtos, ver detalhes de produtos e fazer uma oferta caso ainda seja possível.

Em cada tarefa você vai encontrar um resumo de funcionalidades que devem ser implementadas em seu projeto. Ao final existem algumas funcionalidades consideradas bônus. Você está totalmente livre para decidir a ordem em que vai codificar as tarefas, mas lembre-se que ao fim do prazo esperamos ter uma aplicação funcional, ou seja, que permita ao usuário navegar e usar seus recursos, mesmo que algumas tarefas não tenham sido executadas.

## Entidades de BD
Lote (lot), Lance (bid), Usuário (User), Produto (product) 

## Funcionalidades
A prioridade de implementação obedece essa ordem abaixo (em evolução com o projeto):

- Visualizar Lotes (DONE)

- Configurar Lotes

- Autenticar administradores e usuários regulares / visitantes

- Usuário regular / visitante
  - Fazer um lance
  - Verificar Lotes Vencidos

- Administrador 
  - Validar Resultados

- Administrador  
  - Cadastrar itens para leilão