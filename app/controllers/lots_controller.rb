class LotsController < ApplicationController
  def show
    @lot = Lot.find(params[:id])
  end

  def new
    @lot = Lot.new
  end

  def create
    @lot = Lot.new(lot_params)

    if @lot.save
      flash[:notice] = "Lote cadastrado com sucesso!"
      redirect_to root_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  private
    def lot_params
      params.require(:lot).permit(:code, :end_date, :minimum_value, :items, :start_date)
    end
    
end