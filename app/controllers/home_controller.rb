class HomeController < ApplicationController
  
  def index  
    lots_all = Lot.all

    @lots = []
    @next_lots = []
    @out_lots = []
    today = DateTime.now.to_date
    lots_all.each do |l|
      start_date = l.start_date.to_date             
      if start_date.after? today
        @next_lots << l 
      else
        end_date = l.end_date.to_date
        if end_date.before? today
          @out_lots << l 
        else
          @lots << l
        end
      end      
    end


  end  
end