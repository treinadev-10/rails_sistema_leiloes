class ProductsController < ApplicationController
  #before_action
  
  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)

    if @product.save
      #flash[:notice] = "Produto cadastrado com sucesso!"
      redirect_to root_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  private

  def product_params
    params.require(:product).permit(:name, :description, :photo, :weight, :dimensions, :product_category)
  end

end