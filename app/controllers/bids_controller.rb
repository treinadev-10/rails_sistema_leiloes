class BidsController < ApplicationController
  before_action :authenticate_user!, only: [:new,:create]

  def show
    @bids = Bid.all
  end

  def new  
    @bid = Bid.new
    @bid.lots << @lot.code
  end

  def create
    @bid = Bid.new(bid_params)

    if @bid.save
      flash[:notice] = "Lance feito com sucesso!"
      redirect_to lot_path(@bid)
    else
      render :new, status: :unprocessable_entity
    end
  end

  private
    def bid_params
      params.require(:bid).permit(:lot_code, :cpf, :bid_value)
    end

end