class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  # validates :cpf, presence: true
  # validates :cpf,  length: { is: 11 }

  def cpf_valid?
    return false if cpf_has_not_only_number
    cpf_is_OK
  end

  def cpf_has_not_only_number
    digits = cpf.split("")
    digits.select {|z| %w(0 1 2 3 4 5 6 7 8 9).include? z}.size == 11
  end

  def cpf_is_OK
    cpf_digits = cpf.split("")[0..8]
    sequence = [10,9,8,7,6,5,4,3,2]
    index = 0
    dig_sum = 0
    cpf_digits.each do |cpf_d|
      dig_sum = dig_sum + cpf_d.to_i * sequence[index]
      index = index + 1
    end
    #subtrair o resto da divisão por 11 do número 11
    sub_11 = 11 - (dig_sum % 11)
    #primeiro dígito verificador
    first_ver = 0
    if sub_11 < 10
      first_ver = sub_11
    else
      first_ver = 0
    end
    
    #dez primeiros dígitos * sequência de 11 à 2 
    #e somamos esse resultado
    cpf_digits = cpf.split("")[0..9]
    sequence = [11,10,9,8,7,6,5,4,3,2]
    index = 0
    dig_sum = 0
    cpf_digits.each do |cpf_d|
      dig_sum = dig_sum + cpf_d.to_i * sequence[index]
      index = index + 1
    end
    #subtrair o resto da divisão por 11 do número 11
    sub_11 = 11 - (dig_sum % 11)
    #primeiro dígito verificador
    second_ver = 0
    if sub_11 < 10
      second_ver = sub_11
    else
      second_ver = 0
    end

    #confirmação dos primeiro e segundo digitos verificadores
    first_OK = (cpf.split("")[9].to_i == first_ver)
    second_OK = (cpf.split("")[10].to_i == second_ver)    
    status_OK = (first_OK) && (second_OK)
  end
               
end
