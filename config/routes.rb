Rails.application.routes.draw do
  devise_for :users
  root "home#index"
  resources :lots, only: [:show,:new,:create,:edit,:update] do
    resources :bids, only: [:show,:new,:create]
  end
  resources :products, only: [:new,:create]
end
