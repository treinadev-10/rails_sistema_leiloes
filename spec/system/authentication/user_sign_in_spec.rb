require 'rails_helper'

describe 'Usuário visita tela inicial' do
  it 'e se autentica com sucesso' do
    #Arrange
    User.create!(cpf: '53840674620', email: 'andre@email.com', password: '123456')

    #Act
    visit root_path
    click_on 'Entrar'
    within('form') do
      fill_in 'E-mail', with: 'andre@email.com'
      fill_in 'Senha', with: '123456'
      click_on 'Autenticar'
    end

    #Assert
    expect(page).to have_content('Signed in successfully.')
    within('nav') do
      expect(page).not_to have_link 'Autenticar'
      expect(page).to have_button 'Sair'
      expect(page).to have_content 'andre@email.com'
    end    
  end    
  
  it 'e autenticado clica em Sair, voltando para a tela inicial' do
    #Arrange
    User.create!(cpf: '53840674620', email: 'andre@email.com', password: '123456')

    #Act
    visit root_path
    within('nav') do
      click_on 'Entrar'      
    end
    within('form') do
      fill_in 'E-mail', with: 'andre@email.com'
      fill_in 'Senha', with: '123456'
      click_on 'Autenticar'
    end
    within('nav') do
      click_on 'Sair'
    end

    #Assert
    expect(current_path).to eq(root_path)
    expect(page).to have_content 'Signed out successfully.'
    within('nav') do
      expect(page).not_to have_link 'Sair'
      expect(page).to have_link 'Entrar'
      expect(page).not_to have_content 'andre@email.com'
    end

  end
end