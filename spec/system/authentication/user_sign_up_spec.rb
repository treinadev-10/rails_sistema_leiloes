require 'rails_helper'

describe 'Usuário cria uma conta' do
  it 'com sucesso' do
    #Arrange

    #Act
    visit root_path
    click_on 'Entrar'
    click_on 'Criar uma conta'
    fill_in 'CPF', with: '53840674620'
    fill_in 'E-mail', with: 'ela@she'
    fill_in 'Senha', with: '555666'
    fill_in 'Confirme a senha', with: '555666'
    click_on 'Criar conta'

    #Assert
    expect(page).to have_content 'Welcome! You have signed up successfully.'
    expect(page).to have_content 'ela@she'
    expect(page).to have_button 'Sair'
  end
end
