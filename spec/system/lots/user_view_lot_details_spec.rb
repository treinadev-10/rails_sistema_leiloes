require 'rails_helper'

describe 'Usuário vê detalhes de um lote disponível' do
  it 'e vê informações adicionais' do
    #Arrange
    Lot.create!(code: 'XYZ123456', start_date: 1.month.ago, end_date: 1.month.after, 
                              minimum_bid: '200', bid_increment: '2', product: 1, 
                              cpf_register: 1, cpf_approve: 2)
    Lot.create!(code: 'TWU000001', start_date: 2.months.ago, end_date: 3.months.after, 
                              minimum_bid: '800', bid_increment: '5', product: 2, 
                              cpf_register: 2, cpf_approve: 1)

    #Act
    visit root_path
    click_on 'TWU000001'

    #Assert
    expect(page).to have_content('Código do lote TWU000001')
    expect(page).to have_content('Lance inicial (R$): --')
    expect(page).to have_content("data início do leilão: #{2.months.ago.to_date}")
    expect(page).to have_content("data final para fazer lances: #{3.months.after.to_date}")

  end

  it 'e volta para a tela inicial' do
    #Arrange
    Lot.create!(code: 'XYZ123456', start_date: 1.month.ago, end_date: 1.month.after, 
                              minimum_bid: '200', bid_increment: '2', product: 1, 
                              cpf_register: 1, cpf_approve: 2)
    Lot.create!(code: 'TWU000001', start_date: 2.months.ago, end_date: 3.months.after, 
                              minimum_bid: '800', bid_increment: '5', product: 2, 
                              cpf_register: 2, cpf_approve: 1)

    #Act
    visit root_path
    click_on 'XYZ123456'
    click_on 'Voltar'

    #Assert
    expect(current_path).to eq(root_path)

  end

end

describe 'Usuário vê detalhes de um lote disponível em breve' do
  it 'e vê informações adicionais' do
    #Arrange
    Lot.create!(code: 'XYZ123456', start_date: 1.month.after, end_date: 2.months.after, 
                              minimum_bid: '200', bid_increment: '2', product: 1, 
                              cpf_register: 1, cpf_approve: 2)
    Lot.create!(code: 'TWU000001', start_date: 2.months.after, end_date: 3.months.after, 
                              minimum_bid: '800', bid_increment: '5', product: 2, 
                              cpf_register: 2, cpf_approve: 1)

    #Act
    visit root_path
    click_on 'XYZ123456'

    #Assert
    expect(page).to have_content('Código do lote XYZ123456')
    expect(page).to have_content('Lance inicial (R$): --')
    expect(page).to have_content("data início do leilão: #{1.month.after.to_date}") 
    expect(page).to have_content("data final para fazer lances: #{2.months.after.to_date}")

  end

  it 'e volta para a tela inicial' do
    #Arrange
    Lot.create!(code: 'XYZ123456', start_date: 1.month.after, end_date: 2.months.after, 
                              minimum_bid: '200', bid_increment: '2', product: 1, 
                              cpf_register: 1, cpf_approve: 2)
    Lot.create!(code: 'TWU000001', start_date: 2.months.after, end_date: 3.months.after, 
                              minimum_bid: '800', bid_increment: '5', product: 2, 
                              cpf_register: 2, cpf_approve: 1)

    #Act
    visit root_path
    click_on 'XYZ123456'
    click_on 'Voltar'

    #Assert
    expect(current_path).to eq(root_path)

  end

end