require 'rails_helper'

describe 'Usuário cadastra um lote' do
  
  it 'a partir da tela inicial' do
    #Arrange

    #Act
    visit root_path
    click_on 'Cadastrar Lote'

    #Assert
    expect(page).to have_field('Código do Lote')
    expect(page).to have_field('Data final')
    expect(page).to have_field('Lance mínimo')
    expect(page).to have_field('Data início')
    expect(page).to have_field('Itens')
  end

  it 'com sucesso' do
    #Arrange

    #Act
    visit root_path
    click_on 'Cadastrar Lote'
    fill_in 'Código do Lote', with: 'XYZ123456'
    fill_in 'Data final', with: '17/05/2023'
    fill_in 'Lance mínimo', with: '200'
    fill_in 'Data início', with: '02/07/2023'
    fill_in 'Itens', with: 'garrafas, panelas'
    click_on 'Enviar'

    #Assert
    expect(current_path).to eq(root_path)
    expect(page).to have_content('código: XYZ123456')

  end

end