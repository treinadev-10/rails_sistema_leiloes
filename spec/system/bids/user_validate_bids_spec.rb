require 'rails_helper'

describe 'Usuário visualiza lotes cuja data limite já foi ultrapassada' do
  it 'se estiver autenticado' do
    #Arrange
    Lot.create!(code: 'XYZ123456', start_date: '02/02/2023', end_date: '01/10/2023', 
                              minimum_bid: '200', items: 'garrafas')

    #Act
    visit root_path
    click_on 'XYZ123456'
    click_on 'Fazer um lance'
    
    #Assert
    expect(current_path).to eq new_user_session_path

  end

  it 'e vê detalhes do lance vencedor' do
    #Arrange
    User.create!(cpf: '53840674620', email: 'eu@me', password: '987654')
    Lot.create!(code: 'XYZ123456', start_date: '02/02/2023', end_date: '01/05/2023', 
                minimum_bid: '200', items: 'garrafas')
    Bid.create!(lot_code: 'XYZ123456',cpf: '53840674620',bid_value: '210')

    #Act
    visit root_path
    click_on 'Entrar'
    within('form') do
      fill_in 'E-mail', with: 'eu@me'
      fill_in 'Senha', with: '987654'
      click_on 'Autenticar'
    end 
    within("section#outlots") do
      click_on 'XYZ123456'
    end
    click_on 'Validar lance vencedor'

    #Assert
    expect(page).to have_link 'Lote XYZ123456'
    expect(page).to have_content 'Vencedor CPF: 53840674620'    
    expect(page).to have_button 'Encerrar lote'
  end

  it 'e encerra com sucesso o lote que tem um lance' do
    #Arrange
    User.create!(cpf: '53840674620', email: 'eu@me', password: '987654')
    Lot.create!(code: 'XYZ123456', start_date: '02/02/2023', end_date: '01/05/2023', 
                minimum_bid: '200', items: 'garrafas')
    Bid.create!(lot_code: 'XYZ123456',cpf: '53840674620',bid_value: '210')

    #Act
    visit root_path
    click_on 'Entrar'
    within('form') do
      fill_in 'E-mail', with: 'eu@me'
      fill_in 'Senha', with: '987654'
      click_on 'Autenticar'
    end
    click_on 'XYZ123456'
    click_on 'Validar lance vencedor'
    click_on 'Encerrar lote'

    #Assert
    expect(current_path).to eq(root_path)
    #expect(page).not_to have_content 'XYZ123456'
  end

  it 'e encerra com sucesso o lote que tem dois lances' do
    #Arrange
    User.create!(cpf: '53840674620', email: 'eu@me', password: '987654')
    Lot.create!(code: 'XYZ123456', start_date: '02/02/2023', end_date: '01/05/2023', 
                minimum_bid: '200', items: 'garrafas')
    Bid.create!(lot_code: 'XYZ123456',cpf: '53840674620',bid_value: '210')
    Bid.create!(lot_code: 'XYZ123456',cpf: '08485432975',bid_value: '250')
    #Act
    #Assert
  end


  it 'e vê lote que não recebeu nenhum lance' do
    #Arrange
    User.create!(cpf: '53840674620', email: 'eu@me', password: '987654')
    Lot.create!(code: 'AAA000999', start_date: '02/02/2023', end_date: '01/05/2023', 
                minimum_bid: '200', items: 'panelas')
    #Act
    visit root_path
    click_on 'Entrar'
    within('form') do
      fill_in 'E-mail', with: 'eu@me'
      fill_in 'Senha', with: '987654'
      click_on 'Autenticar'
    end
    click_on 'AAA000999'
    click_on 'Validar lance vencedor'

    #Assert
    expect(page).to have_content 'Vencedor CPF: Nenhum'    
    expect(page).to have_button 'Encerrar lote'
  end

  it 'e cancela com sucesso lote sem lance' do
    #Arrange
    User.create!(cpf: '53840674620', email: 'eu@me', password: '987654')
    Lot.create!(code: 'AAA000999', start_date: '02/02/2023', end_date: '01/05/2023', 
                minimum_bid: '200', items: 'panelas')
    #Act
    visit root_path
    click_on 'Entrar'
    within('form') do
      fill_in 'E-mail', with: 'eu@me'
      fill_in 'Senha', with: '987654'
      click_on 'Autenticar'
    end
    click_on 'AAA000999'
    click_on 'Validar lance vencedor'
    click_on 'Encerrar lote'

    #Assert
    expect(current_path).to eq(root_path)
    expect(page).not_to have_link 'AAA000999'
  end
end
