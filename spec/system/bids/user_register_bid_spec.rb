require 'rails_helper'

describe 'Usuário regular visualiza detalhes do lote disponível' do
  it 'e está autorizado a fazer um lance' do
    #Arrange
    User.create!(cpf: '53840674620', email: 'eu@me', password: '987654')
    Lot.create!(code: 'XYZ123456', start_date: '02/02/2023', end_date: '01/10/2023', 
    minimum_bid: '200', items: 'garrafas')

    #Act
    visit root_path
    click_on 'Entrar'
    within('form') do
      fill_in 'E-mail', with: 'eu@me'
      fill_in 'Senha', with: '987654'
      click_on 'Autenticar'
    end
    click_on 'XYZ123456'
    click_on 'Fazer um lance'
    
    #Assert    
    expect(page).to have_content('Registrando um lance')
  end

  it 'e faz um lance com sucesso' do
    #Arrange
    User.create!(cpf: '53840674620', email: 'eu@me', password: '987654')
    Lot.create!(code: 'XYZ123456', start_date: '02/02/2020', end_date: '01/01/2024', 
    minimum_bid: '200', items: 'garrafas')
    
    #Act
    visit root_path
    click_on 'Entrar'
    within('form') do
      fill_in 'E-mail', with: 'eu@me'
      fill_in 'Senha', with: '987654'
      click_on 'Autenticar'
    end
    click_on 'XYZ123456'
    click_on 'Fazer um lance'
    fill_in 'Código do lote', with: 'XYZ123456'
    fill_in 'CPF', with: '53840674620'
    fill_in 'Valor do lance', with: '250'
    click_on 'Enviar'
    
    #Assert
    expect(page).to have_content('Lance feito com sucesso!')

  end

end