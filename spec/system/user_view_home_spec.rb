require 'rails_helper'

describe 'Usuário visita tela inicial' do
  
  it 'e vê o nome da app' do
    #Arrange

    #Act
    visit root_path

    #Assert
    expect(page).to have_content('Leilão de produtos')

  end

  it 'e vê lotes disponíveis para leilão' do
    #Arrange    
    Lot.create!(code: 'XYZ123456', start_date: 1.month.ago, end_date: 1.month.after, 
                              minimum_bid: '200', bid_increment: '2', product: 1, cpf_register: 1, cpf_approve: 2)
    Lot.create!(code: 'TWU000001', start_date: 2.months.ago, end_date: 3.months.after, 
                              minimum_bid: '800', bid_increment: '5', product: 2, cpf_register: 2, cpf_approve: 1)
    
    #Act
    visit root_path

    #Assert  
    expect(page).to have_content('Lotes disponíveis para lances:') 
    expect(page).to have_content('código: XYZ123456')    
    expect(page).to have_content('código: TWU000001')    

  end

  it 'e vê que não há lotes disponíveis' do 
    #Arrange

    #Act
    visit root_path

    #Assert
    expect(page).to have_content('Não há lotes disponíveis.')

  end

  it 'e vê lotes disponíveis em breve' do
    #Arrange
    Lot.create!(code: 'AAA101101', start_date: 1.month.after, end_date: 2.months.after, 
                 minimum_bid: '200', bid_increment: '2', product: 1, cpf_register: 1, cpf_approve: 2)
    Lot.create!(code: 'ZZZ901109', start_date: 10.days.from_now, end_date: 40.days.from_now, 
                 minimum_bid: '400', bid_increment: '5', product: 2, cpf_register: 2, cpf_approve: 1)

    #Act
    visit root_path

    #Assert
    expect(page).to have_content('Lotes disponíveis em breve:')
    expect(page).to have_content('código: AAA101101')
    expect(page).to have_content('código: ZZZ901109')  
  end

  it 'e autenticado vê os lotes em andamento e futuro' do
    #Arrange
    User.create!(cpf: '53840674620', email: 'eu@me', password: '987654')
    Lot.create!(code: 'AAA101101', start_date: 1.month.after, end_date: 2.months.after, 
                 minimum_bid: '200', bid_increment: '2', product: 1, cpf_register: 1, cpf_approve: 2)
    Lot.create!(code: 'ZZZ901109', start_date: 10.days.from_now, end_date: 40.days.from_now, 
                 minimum_bid: '400', bid_increment: '5', product: 2, cpf_register: 2, cpf_approve: 1)

    #Act
    visit root_path
    click_on 'Entrar'
    within('form') do
      fill_in 'E-mail', with: 'eu@me'
      fill_in 'Senha', with: '987654'
      click_on 'Autenticar'
    end

    #Assert
    expect(page).to have_content('eu@me')
    expect(page).to have_content('Lotes disponíveis em breve:')
    expect(page).to have_content('código: AAA101101')
    expect(page).to have_content('Lotes disponíveis para lances:')
    expect(page).to have_content('código: ZZZ901109')
  end

end
