require 'rails_helper'

describe 'Usuário deseja cadastrar itens para lotes' do
  it 'e vê tela de cadastro' do
    #Arrange

    #Act
    visit root_path
    click_on 'Cadastrar Produtos'

    #Assert
    expect(page).to have_content 'Cadastro de Itens para Lotes'
  end

  it 'e vê formulário de cadastro' do
    #Arrange

    #Act
    visit root_path    
    click_on 'Cadastrar Produtos'

    #Assert
    expect(page).to have_field 'Nome'
    expect(page).to have_field 'Descrição'
    expect(page).to have_field 'Foto'
    expect(page).to have_field 'Peso'
    expect(page).to have_field 'Dimensões em cm (largura, altura e profundidade)'
    expect(page).to have_field 'Categoria de produto'

  end

  it 'e cadastra item com sucesso' do
    #Arrange

    #Act
    visit root_path
    click_on 'Cadastrar Produtos'
    within('form') do
      fill_in 'Nome', with: 'Televisão'  
      fill_in 'Descrição', with: 'Smart TV 40"' 
      fill_in 'Foto', with: '######' 
      fill_in 'Peso', with: '4'
      fill_in 'Dimensões em cm (largura, altura e profundidade)', with: '(2,3,4)' 
      fill_in 'Categoria de produto', with: 'Eletrodoméstico'
      click_on 'Cadastrar'
    end

    #Assert
    expect(current_path).to eq root_path 
  end

end