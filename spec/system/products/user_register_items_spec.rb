require 'rails_helper'

describe 'Usuário cadastra itens para leilão' do
  it 'e vê essa opção no menu' do
    #Arrange

    #Act
    visit root_path

    #Assert
    expect(page).to have_link 'Cadastrar Itens'
  end
end