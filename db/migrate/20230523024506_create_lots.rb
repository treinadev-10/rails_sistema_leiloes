class CreateLots < ActiveRecord::Migration[7.0]
  def change
    create_table :lots do |t|
      t.string :code
      t.string :minimum_bid
      t.date :start_date
      t.date :end_date
      t.string :bid_increment
      t.integer :bid
      t.integer :product
      t.integer :cpf_register
      t.integer :cpf_approve

      t.timestamps
    end
  end
end
