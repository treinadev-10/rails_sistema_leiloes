class RemoveCpfFromUser < ActiveRecord::Migration[7.0]
  def change
    remove_column :users, :cpf, :integer
  end
end
