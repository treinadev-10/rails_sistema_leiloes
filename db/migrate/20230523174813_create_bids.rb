class CreateBids < ActiveRecord::Migration[7.0]
  def change
    create_table :bids do |t|
      t.string :bid_value
      t.integer :cpf

      t.timestamps
    end
  end
end
